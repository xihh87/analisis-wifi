## Análisis WiFi

[Descripción del proyecto](https://joshua.haase.mx/ibero/2023-05-25-análisis-wifi/ ).

Para configurar el entorno de trabajo:

1.  Iniciar el entorno de trabajo
    ```
    make run
    ```
2.  Desde [la consola de rstudio](http://localhost:8787/ ) ejecutar:
    ```
    setwd("./project")
    install.packages("renv")
    renv::activate()
    renv::install()
    ```

Para repetir el análisis a partir de los datos,
entrar en [la terminal de rstudio](http://localhost:8787/ ) y ejecutar:

```
make data
make analysis
```
