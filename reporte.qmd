---
title: "Análisis de demanda de internet inalámbrico"
author: "Joshua Haase"
format: pdf
editor: source
date: 2023-07-07
---

```{r, setup, echo=FALSE, include=FALSE}
suppressPackageStartupMessages(library('tidyverse'))
suppressPackageStartupMessages(devtools::load_all("lib/this-wifi-analysis/"))
suppressPackageStartupMessages(devtools::load_all("lib/semaphore-heatmap/"))
library('knitr')
knitr::opts_chunk$set(echo = FALSE)
```

![](logo-icd.png)

\newpage

## Introducción

Como parte de los esfuerzos de mejora continua
de la Universidad Iberoamericana Ciudad de México,
el *Centro de Inteligencia de Datos* de la
*Dirección General de Planeación Estratégica e Innovación*,
y la *Coordinación de Ciencia de Datos*
del *Departamento de Estudios en Ingeniería para la Innovación* (DEII),
**analizamos los datos de conexión**
entre puntos de acceso inalámbricos y dispositivos,
**para diagnosticar si existen problemas en el uso de la red,
evaluar y buscar soluciones
que mejoren el servicio de WiFi**
que se ofrece en el campus a la comunidad Ibero.

Se encontró que **se puede optimizar la distribución de los puntos de acceso
para mejorar la calidad del servicio**.

**Es imprescindible la información del uso del ancho de banda
para planificar esta optimización
y emitir recomendaciones**.

**La información solicitada
[podría obtenerse del sistema de monitoreo](https://support.solarwinds.com/SuccessCenter/s/article/Use-SolarWinds-Query-Language-SWQL?language=en_US )**,
eso además **corregiría el sesgo de medición** que se encontró en los datos.


## Metodología

La *Dirección de Informática y Telecomunicaciones
entregó* datos de las *conexiones establecidas* entre
los puntos de acceso de la Universidad Iberoamericana
y los dispositivos de usuarios.

Estos datos se consolidan en un único conjunto de información,
y se realiza la limpieza de datos.

Se realizan análisis descriptivos,
se responden preguntas acerca de las conexiones
y se preparan gráficos para un tablero de monitoreo.

### Se requiere mejorar la calidad de la información

Se realizó la limpieza de datos conjuntando
las conexiones con los puntos de acceso y su ubicación.

Se encontraron inconsistencias entre la forma en que se especifica la ubicación
en las bitácoras de conexiones y el conjunto de datos de los equipos de red inalámbrica
y se tomaron como canónicas las de la base de datos de características de AP.

**Se solicita a la Dirección de Informática y Telecomunicaciones
compartir la ubicación de los puntos de acceso inalámbricos
[en este documento](https://iberomx-my.sharepoint.com/:x:/g/personal/adriana_fajardo_ibero_mx/EXVpKYEBG75JvUWPvE4s9XsBpIbuae6_MHv7tWNbuGQTvQ?e=S4vpHa ) **.
*Falta información acerca de 6 puntos de acceso al final
y el salón al que corresponde cada punto de acceso*.

## La información no es uniforme por lo existe sesgo de medición

```{r, load_data, echo=FALSE, include=FALSE}
access_points_by_time <- load_wifi_data() |>
  summarize_connections()
```

Se cuenta con mediciones de conexión durante
`r access_points_by_time |> pull(Fecha) |> unique() |> length()` días.

Se cuenta con `r access_points_by_time |> pull(Fecha) |> length()` lecturas de los puntos de acceso.

Las lecturas en cada día no son uniformes:

```{r, connection_on_day}
p <- access_points_by_time |>
group_by(Fecha) |>
summarise(Lecturas = n(), .groups="drop_last") |>
ggplot(aes(Fecha, Lecturas)) +
geom_bar(stat="identity")
ticks_by_day(p)
```

La distribución de lecturas no es uniforme entre puntos de acceso:

```{r, connection_on_sensors, fig.width=10, fig.height=4}
access_points_by_time |>
group_by(Sensor) |>
summarise(Lecturas = n(), Día = length(unique(Fecha)), .groups="drop_last") |>
arrange(Lecturas) |>
ggplot(aes(reorder(Sensor, -(Lecturas/Día)), Lecturas/Día)) +
geom_bar(stat="identity") +
theme(axis.text.x = element_blank()) +
xlab("AP (ordenado por media de lecturas al día)")
```

Dado que las lecturas no son uniformes
**los resultados de este análisis incluyen el sesgo de la medición**:

```{r, connection_heatmap, fig.height=10, fig.width=10}
p <- access_points_by_time |>
    group_by(Edificio, Fecha) |>
    summarise(Lecturas = n(), AP=length(unique(Sensor)), .groups="drop_last") |>
    ggplot(aes(Fecha, Edificio, fill=Lecturas/AP)) +
    geom_tile() +
    scale_fill_gradient(low="white", high="black") +
    theme_minimal() +
    theme(
      legend.position = "bottom",
      legend.box = "horizontal",
      legend.text = element_text(angle=45, hjust=1),
    )
ticks_by_day(p)
```

## Es imprescindible información acerca del uso ancho de banda por conexión para emitir recomendaciones

Hay variables relevantes a la calidad del servicio que no se encuentran en el conjunto de datos:

-   **Ancho de banda utilizado por conexión**
-   Canal utilizado por el punto de acceso
-   Retransmisiones

La información solicitada
[podría obtenerse del sistema de monitoreo](https://support.solarwinds.com/SuccessCenter/s/article/Use-SolarWinds-Query-Language-SWQL?language=en_US )
que administra la Dirección de Informática y Telecomunicaciones,
eso además corregiría el sesgo de medición que se encontró en los datos.

## Resultados

### Las conexiones por punto de acceso coinciden con el criterio de diseño

Enrique Montiel nos comentó que
de acuerdo a la información del proveedor,
los puntos de acceso deberían tolerar 35 conexiones.

```{r}
max_conn = 0.8*35
```

Se encontraron `r access_points_by_time |> select(Sensor) |> pull() |> unique() |> length()` puntos de acceso inalámbricos,
distribuidos entre los edificios de la siguiente forma:

```{r, ap_por_edificio, echo=FALSE}
ap_por_edificio <- access_points_by_time |>
  group_by(Edificio) |>
  select(Edificio, Sensor) |>
  unique() |>
  group_by(Edificio) |>
  summarise(
    `Access Points` = n(),
    .groups = "drop_last"
  )
ggplot(ap_por_edificio, aes(reorder(Edificio, -`Access Points`), `Access Points`)) +
geom_bar(stat="identity") +
theme(axis.text.x = element_text(angle=45, hjust=1)) +
xlab("Edificio (ordenado por AP)")
```

Tomando la cantidad máxima de conexiones simultáneas en cada punto de acceso
(escenario pesimista),
se estima que la demanda en cada edificio corresponde a:

```{r, demanda, echo=FALSE}
demanda <- access_points_by_time |>
  select(Edificio, Sensor, `Dispositivos concurrentes`) |>
  group_by(Edificio, Sensor) |>
  summarise(
    `Dispositivos concurrentes` = max(`Dispositivos concurrentes`),
    .groups = "drop_last"
  ) |>
  group_by(Edificio) |>
  summarise(
    `Dispositivos concurrentes` = sum(`Dispositivos concurrentes`)
  ) |>
  arrange(desc(`Dispositivos concurrentes`))

demanda |>
ggplot(aes(reorder(Edificio, -`Dispositivos concurrentes`), `Dispositivos concurrentes`)) +
geom_bar(stat="identity") +
theme(axis.text.x = element_text(angle=45, hjust=1)) +
xlab("Edificio (ordenado por demanda)")
```

Eso corresponde a una *demanda normalizada* (conexiones / puntos de acceso)
en cada edificio:

```{r, demanda_normalizada, echo=FALSE}
demanda |>
  left_join(ap_por_edificio, by="Edificio") |>
  mutate(
    `Demanda normalizada` = `Dispositivos concurrentes` / `Access Points`
  ) |>
  select(
    Edificio, `Demanda normalizada`
  ) |>
  arrange(desc(`Demanda normalizada`)) |>
  ggplot(aes(reorder(Edificio, -`Demanda normalizada`), `Demanda normalizada`)) +
  geom_bar(stat="identity") +
  xlab("Edificio (ordenado por demanda normalizada)") +
  theme(axis.text.x = element_text(angle=45, hjust=1))
```

Salvo por las puertas y E6,
se encuentra que **la carga media de conexiones se considera aceptable**.
Dado que las puertas y E6 están en el exterior,
se puede suponer que
**gente fuera de la Ibero intenta establecer conexión con los puntos de acceso exteriores**
y esto podría degradar el servicio en esas ubicaciones.

Sin embargo, **esta conclusión debe complementarse con el uso de ancho de banda por conexión**.

### Pocos puntos de acceso concentran una gran cantidad de conexiones

Verificando la relación entre conexiones y horario,
se encontró que los puntos de acceso de las puertas tienen
una cantidad mayor de conexiones en todos los horarios.

En el siguiente gráfico se muestran como puntos
los valores anormales:

```{r, all_traffic, echo=FALSE, fig.width=10, fig.height=10}
access_points_by_time |>
  mutate(
    hora=hour(Hora)
  ) |>
  ggplot(aes(x=Hora, y=`Dispositivos concurrentes`)) +
  geom_boxplot(aes(group=hora), outlier.alpha=0.1) +
  theme(axis.text.x = element_text(angle=45, hjust=1)) +
  scale_y_continuous(limits=c(0, 300), breaks=c(0, 150, 300)) +
  facet_wrap(~Edificio)
```

Del gráfico anterior se desprende la conclusión de que
las conexiones no se distribuyen de manera uniforme,
sino que una minoría de puntos de acceso tienen la mayoría de la carga,
**hay momentos puntuales con muchas conexiones en pocos puntos de acceso**.

Eliminando los valores extremos y los puntos de acceso externo,
se grafican las conexiones por horarios.
La línea más gruesa es la mediana,
y las línea más alta representa el valor que abarca 90%
de las mediciones:

```{r, normal_load, echo=FALSE, warning=FALSE, fig.height=10, fig.width=10}
access_points_by_time |>
  mutate(
    hora=hour(Hora)
  ) |>
excluir_edificios_externos() |>
ggplot(
  aes(x=Hora, y=`Dispositivos concurrentes`),
) +
  geom_boxplot(aes(group=hora), outlier.alpha=0.1) +
  theme(axis.text.x = element_text(angle=45, hjust=1)) +
  facet_wrap(~Edificio)
```

El siguiente paso del análisis es encontrar esos puntos de acceso.

```{r, variance_in_sensor, echo=FALSE}
manual_boxplot <- access_points_by_time |>
  group_by(Edificio, Sensor) |>
  summarise(
    q1 = quantile(`Dispositivos concurrentes`, 0.25),
    q2 = median(`Dispositivos concurrentes`),
    q3 = quantile(`Dispositivos concurrentes`, 0.75),
    p90 = quantile(`Dispositivos concurrentes`, 0.90),
    max = max(`Dispositivos concurrentes`),
    n = n(),
    .groups = "drop_last"
  ) |>
  arrange(-p90, -q3, -q2)
```

```{r, problems_in_connections}
problems_by_day <- access_points_by_time |>
  group_by(Fecha, Edificio, Sensor) |>
  summarise(
    n = n(),
    problemas = sum(`Dispositivos concurrentes` > max_conn),
    `Proporción de problemas` = sum(`Dispositivos concurrentes` > max_conn) / n(),
    .groups = "drop_last"
  ) |>
  arrange(Edificio, Sensor, Fecha)

problems_by_sensor_and_day <- access_points_by_time |>
  group_by(Edificio, Sensor, Fecha) |>
  summarise(
    n = n(),
    problemas = sum(`Dispositivos concurrentes` > max_conn),
    `Proporción de problemas` = sum(`Dispositivos concurrentes` > max_conn) / n(),
    .groups = "drop_last"
  ) |>
  arrange(Edificio, Sensor, Fecha)

problems_global <- access_points_by_time |>
  group_by(Edificio, Sensor) |>
  summarise(
    n = n(),
    problemas = sum(`Dispositivos concurrentes` > max_conn),
    `Proporción de problemas` = sum(`Dispositivos concurrentes` > max_conn) / n(),
    .groups = "drop_last"
  ) |>
  arrange(desc(`Proporción de problemas`))

problems_by_building_and_date <- access_points_by_time |>
  group_by(Edificio, Fecha) |>
  summarise(
    n = n(),
    problemas = sum(`Dispositivos concurrentes` > max_conn),
    `Proporción de problemas` = sum(`Dispositivos concurrentes` > max_conn) / n(),
    .groups = "drop_last"
  ) |>
  arrange(desc(`Proporción de problemas`))

problems_by_building <- access_points_by_time |>
  group_by(Edificio) |>
  summarise(
    n = n(),
    problemas = sum(`Dispositivos concurrentes` > max_conn),
    `Proporción de problemas` = sum(`Dispositivos concurrentes` > max_conn) / n(),
    .groups = "drop_last"
  ) |>
  arrange(desc(`Proporción de problemas`), desc(n))
```


### Puntos de acceso sobre-utilizados

Se encuentra que en
`r manual_boxplot |> filter(p90 <= 35) |> pull(Sensor) |> length()`
de `r manual_boxplot |> pull(Sensor) |> length()`
puntos de acceso tienen menos de 35 conexiones
en 90% de las mediciones.

Esta conclusión **debe confirmarse con
lecturas uniformes a lo largo del tiempo en los puntos de acceso
durante el semestre
y mediciones de ancho de banda**.

Los `r manual_boxplot |> filter(p90 > 35) |> pull(Sensor) |> length()` puntos de acceso
con más conexiones son:

```{r, sensors_with_more_connections, echo=FALSE}
manual_boxplot |>
  ungroup() |>
  excluir_edificios_externos() |>
  arrange(desc(max)) |>
  slice_max(max, n=12) |>
  kable()
```

### Puntos de acceso sub-utilizados (podría ser sesgo de medición)

`r limite_sub_utilizacion <- 5`

Se encuentra que en
`r manual_boxplot |> filter(p90 < limite_sub_utilizacion) |> pull(Sensor) |> length()`
de `r manual_boxplot |> pull(Sensor) |> length()`
puntos de acceso tienen menos de `r limite_sub_utilizacion` conexiones
en 90% de las mediciones,
sugiriendo que están sub-utilizados (ver Anexo).

Esta conclusión **debe confirmarse con
lecturas uniformes a lo largo del tiempo en los puntos de acceso
durante el semestre y mediciones de ancho de banda**.

## Se presenta ejemplo de tablero de información

Se diseñó un esquema para presentar la información,
que equilibra la cantidad de información que se requiere almacenar
y la capacidad descriptiva del tablero
que consiste en un mapa de calor con varios niveles de análisis.

El esquema de color de estas imágenes se adaptó para identificar fácilmente:

-   Las zonas de **sub-utilización (blanco)**.
-   Las zonas de uso óptimo, 80% de capacidad de diseño (verde).
-   Las zonas en capacidad máxima de conexión (rojo).
-   Las **zonas con problemas (morado)**.

Se presenta un resumen de la peor calidad del servicio registrada
por edificio en una fecha determinada a partir de la información que se tiene:

```{r, resumen_por_día_y_edificio, fig.width=7, fig.height=7}
access_points_by_time |>
   summarize_by_day_and_building() |>
   excluir_edificios_externos() |>
   mutate(Fecha = as.Date(Fecha)) |>
   ggplot(aes(Fecha, Edificio, fill = `Dispositivos concurrentes`)) |>
   semaphore_heatmap() |>
   ticks_by_day()
```

Este análisis se puede repetir por edificio y piso:

```{r, resumen_por_día_en_edificio_n}
p <- access_points_by_time |>
  summarize_by_day_building_and_floor("N") |>
  ggplot(aes(Fecha, Piso, fill=`Dispositivos concurrentes`)) |>
  semaphore_heatmap() |>
  ticks_by_day()
p + labs(title="Conexiones en edificio N")
```

Para el piso de un edificio en una fecha particular:

```{r, resumen_por_día_en_edificio_n_piso_1_2023-04-20}
p <- access_points_by_time |>
  show_building_floor_on_date(building = "N", floor = "0", date = "2023-04-20") |>
  ggplot(aes(Hora, Sensor, fill=`Dispositivos concurrentes`)) |>
  semaphore_heatmap() +
  theme(axis.text.x = element_text(angle=45, hjust=1))
p + labs(title="Conexiones en edificio N, PB, 2023-04-20")
```

Y para una hora en particular:

```{r, building_floor_hour}
p <- access_points_by_time |>
  show_building_floor_on_date_hour("N", "0", "2023-04-20", "20") |>
  ggplot(aes(Hora, Sensor, fill=`Dispositivos concurrentes`)) |>
  semaphore_heatmap() +
  theme(axis.text.x = element_text(angle=45, hjust=1))
p + labs(title="Conexiones en edificio N, PB, 2023-04-20 20:00")
```

```{r, timelines, echo=FALSE}
timelines <- access_points_by_time |>
  excluir_edificios_externos() |>
  mutate(Tiempo = ymd(Fecha)+hms(Hora))
```

\newpage

## Hay zonas donde las conexiones exceden el criterio de diseño recurrentemente

Para ubicar de manera espacial dónde se concentra
la mayor cantidad de conexiones:

```{r, timelines_grid_up, echo=FALSE, fig.height=10, fig.width=10}
max_conn = 0.8*35
timelines |>
excluir_edificios_externos() |>
filter(`Dispositivos concurrentes` > max_conn) |>
  ggplot(
    aes(
      Tiempo,
      `Dispositivos concurrentes`,
      color = `Dispositivos concurrentes`
    )
  ) +
  geom_point(color="red", alpha=0.1) +
  geom_hline(yintercept = max_conn) +
  facet_grid(Edificio ~ as.numeric(Piso)) +
  theme(
    legend.position = "none",
    axis.text = element_blank(),
    axis.ticks = element_blank()
  )
```

\newpage

Para ubicar de manera espacial dónde
las conexiones están en un nivel aceptable:

```{r, timelines_grid_down, echo=FALSE, fig.width=10, fig.height=10}
max_conn = 0.8*35
timelines |>
excluir_edificios_externos() |>
filter(`Dispositivos concurrentes` < max_conn) |>
  ggplot(
    aes(
      Tiempo,
      `Dispositivos concurrentes`,
      color = `Dispositivos concurrentes`
    )
  ) +
  geom_point(color="blue", alpha = 0.05) +
  geom_hline(yintercept = max_conn) +
  facet_grid(Edificio ~ as.numeric(Piso)) +
  theme(
    legend.position = "none",
    axis.text = element_blank(),
    axis.ticks = element_blank()
  )
```

\newpage

## Propuesta para continuar el trabajo de optimización de la red

-   [ ] La *Dirección de Informática y Telecomunicaciones* comparte acceso
    a la información a través de Solar Winds Query Language
    (conexiones, ancho de banda por conexión).
-   [ ] En conjunto, el *Centro de Inteligencia de Datos de la
    Dirección General de Planeación Estratégica e Innovación*,
    y la *Coordinación de Ciencia de Datos
    del Departamento de Estudios en Ingeniería para la Innovación (DEII)*
    generan un tablero de información
    con indicadores de estado de salud de la red
    segmendado por ubicación espacio-temporal.
-   [ ] El *Centro de Inteligencia de Datos de la
    Dirección General de Planeación Estratégica e Innovación*
    emite recomendaciones para mejorar la distribución de los puntos de acceso.
-   [ ] La *Dirección de Informática y Telecomunicaciones*
    redistribuye los puntos de acceso
    y utiliza el tablero de información para evaluar la mejora.

\newpage

# Anexos

## Anexo 1: Puntos de acceso sub-utilizados

`r limite_sub_utilizacion<-5`

Los puntos de acceso sub-utilizados se definen
como los que en el 90% de sus lecturas tienen menos de `r limite_sub_utilizacion` conexiones.

Son los siguientes:

```{r, sensors_with_less_connections, echo=FALSE}
manual_boxplot |>
  filter(p90 < limite_sub_utilizacion) |>
  arrange(max) |>
  kable()
```

## Anexo 2: Puntos de Acceso (AP) con problemas

```{r}
problems_global |>
  filter(`problemas` > 0) |>
  excluir_edificios_externos() |>
  arrange(desc(`Proporción de problemas`)) |>
  kable()
```
