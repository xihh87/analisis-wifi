summarize_by_day_all_sensors_in_building <- function(datum, building) {
  datum |>
    dplyr::filter(Edificio == building) |>
    dplyr::group_by(Sensor, Fecha) |>
    dplyr::summarise(
      `Dispositivos concurrentes` = max(`Dispositivos concurrentes`),
      .groups = "drop_last"
    ) |>
    dplyr::mutate(
      `Dispositivos concurrentes` = ifelse(
        `Dispositivos concurrentes` > 90,
        90,
        `Dispositivos concurrentes`
      ),
      Fecha = lubridate::ymd(Fecha)
    )
}
