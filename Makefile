help:	## show this message
	@cmd/show-help Makefile
data:	## download available data from the internet
	@mkdir -p data/clean
	@wget -O data/clean/2023-06-15-students.txz https://files.haase.mx/ibero/wifi/2023-06-15-students.txz
	@wget -O data/clean/2023-06-15-access_points.tsv https://files.haase.mx/ibero/wifi/2023-06-15-access_points.tsv
	@tar -xf data/clean/2023-06-15-students.txz
analysis:	analysis/2023-06-23-access-points-by-time.tsv	## re-do the analysis from code
	@true
run:	## build analysis environment
	docker compose up -d
stop:	## stop analysis environment
	docker compose stop
analysis/2023-06-23-access-points-by-time.tsv:	data/clean/2023-06-15-students.tsv	data/clean/2023-06-15-access_points.tsv
	mkdir -p analysis
	cmd/002-plot-access-points
.PHONY: analysis data help run stop
